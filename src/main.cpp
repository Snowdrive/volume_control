// ! This is as far as I'm gonna go with this project, 
// ! I can't figure out a way to make this thing last longer than 48 hrs 
// ! on a single 18650 while also staying responsive at all times
// ! I'll just remake it on a wired Arduino that has USB capability

#include <Arduino.h>

// ! Define rotary pins
#define CLK 18
#define DT 19
#define SW 21
#define PW 15

// Include required libs
#include <WiFi.h>
#include "driver/adc.h"
#include <BleKeyboard.h>
#include "esp32-hal-cpu.h"

// Intiailize BLE HID Emulator with name
BleKeyboard bleKeyboard("Volume Control");

// Variables for rotary encoder
int currentStateCLK;
int lastStateCLK;
String currentDir ="";
unsigned long lastButtonPress = 0;

// Current output device, 0 is speakers, 1 is headphones
int currentDevice = 1;

// ? Disabled for power saving
//int connectionAnnounced = 0;

void setup() {
	
  	// ! I don't think this does much to help save power
  	setCpuFrequencyMhz(80);
	adc_power_off();
	WiFi.disconnect(true);
	WiFi.mode(WIFI_OFF);

	pinMode(CLK,INPUT);
	pinMode(DT,INPUT);
	pinMode(SW, INPUT_PULLUP);
	pinMode(PW, OUTPUT);

	// ? Disabled for power saving
	// Serial.begin(115200);

	lastStateCLK = digitalRead(CLK);

  bleKeyboard.begin();

  // ? Disabled for power saving
  // Serial.println("Device Started!");
}

void loop() {

  // ? Disabled for power saving
  // if(connectionAnnounced == 0 && bleKeyboard.isConnected()){
  //   Serial.println("Connected!");
  //   connectionAnnounced = 1;
  // }

	currentStateCLK = digitalRead(CLK);

	if (currentStateCLK != lastStateCLK  && currentStateCLK == 1){

		if (digitalRead(DT) != currentStateCLK) {
			currentDir ="UP";
		} else {
			currentDir ="DOWN";
		}

		// ? Disabled for power saving
		// Serial.print("Sending Signal: ");
		// Serial.println(currentDir);

    if (currentDir == "UP"){
    bleKeyboard.press(KEY_MEDIA_VOLUME_UP);
    bleKeyboard.release(KEY_MEDIA_VOLUME_UP);
    }

    if (currentDir == "DOWN"){
    bleKeyboard.press(KEY_MEDIA_VOLUME_DOWN);
    bleKeyboard.release(KEY_MEDIA_VOLUME_DOWN);
    }
	}

	lastStateCLK = currentStateCLK;

	int btnState = digitalRead(SW);

	if (btnState == LOW) {
		if (millis() - lastButtonPress > 50) {
			// ? Disabled for power saving
			// Serial.println("Sending Signal: SWITCH");
			if (currentDevice == 1) {
				// ? Send LAlt+F1 for SoundSwitch to change output to speakers
				bleKeyboard.press(KEY_LEFT_ALT);
				bleKeyboard.press(KEY_F1);
				bleKeyboard.release(KEY_LEFT_ALT);
				bleKeyboard.release(KEY_F1);
				// ? Send LCtrl + LAlt + F1 for EqAPO to change profile to speakers
				bleKeyboard.press(KEY_LEFT_ALT);
				bleKeyboard.press(KEY_LEFT_CTRL);
				bleKeyboard.press(KEY_F1);
				bleKeyboard.release(KEY_LEFT_ALT);
				bleKeyboard.release(KEY_LEFT_CTRL);
				bleKeyboard.release(KEY_F1);

				currentDevice = 0;
			} else {
				// ? Send LAlt+F2 for SoundSwitch to change output to headphones
				bleKeyboard.press(KEY_LEFT_ALT);
				bleKeyboard.press(KEY_F2);
				bleKeyboard.release(KEY_LEFT_ALT);
				bleKeyboard.release(KEY_F2);
				// ? Send LCtrl + LAlt + F2 for EqAPO to change profile to headphones
				bleKeyboard.press(KEY_LEFT_ALT);
				bleKeyboard.press(KEY_LEFT_CTRL);
				bleKeyboard.press(KEY_F2);
				bleKeyboard.release(KEY_LEFT_ALT);
				bleKeyboard.release(KEY_LEFT_CTRL);
				bleKeyboard.release(KEY_F2);

				currentDevice = 1;
			}
		}

		lastButtonPress = millis();
	}

	// ? Debounce rotary inputs
	delay(1);
}